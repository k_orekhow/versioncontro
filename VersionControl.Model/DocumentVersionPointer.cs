﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VersionControl.Model
{
    public class DocumentVersionPointer
    {
        public int Id { get; set;  }
        public int FirstVersionId { get; set; }
        public int ActualVersionId { get; set; }
       
    }
}
