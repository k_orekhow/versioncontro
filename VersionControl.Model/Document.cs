﻿namespace VersionControl.Model
{
    public class Document
    {
        public int Id { get; set; }
        public int FirstVersionId { get; set; }
        public int DocumentVersion { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Hash { get; set; }
    }
}
