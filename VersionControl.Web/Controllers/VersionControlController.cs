﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using DomainModel=VersionControl.DomainModel;
using VersionControl.Infrastructure;
using VersionControl.Web.ErrorHandling;

namespace VersionControl.Web.Controllers
{
    [WebApiExceptionFilter(Type = typeof(Exception), StatusCode = HttpStatusCode.NotAcceptable)]
    [RoutePrefix("api/v1")]
    public class VersionControlController:ApiController
    {
        private IDocumentService _documentService;
        public VersionControlController(IDocumentService documentService)
        {
            _documentService = documentService;
        }

        [HttpGet]
        [Route("documents")]
        public IHttpActionResult GetAllActualDocs()
        {
            var actualDocuments = _documentService.GetActualDocuments();

            return Ok(actualDocuments);
        }

        [HttpPost]
        [Route("documents/versions")]
        public IHttpActionResult GetDocumentVersionList([FromBody]DomainModel::Document document)
        {
            IList<DomainModel.Document> documents = _documentService.GetDocumentVersionList(document);

            return Ok(documents);
        }

        [HttpPost]
        [Route("documents/setactual")]
        public IHttpActionResult SetActual([FromBody]DomainModel::Document document)
        {
            var actual = _documentService.SetActual(document);

            return Ok(actual);
        }

        [HttpPost]
        [Route("documents/getactual")]
        public IHttpActionResult GetActualVersion([FromBody]DomainModel::Document document)
        {
            var actualVersion = _documentService.GetActualVersion(document);

            return Ok(actualVersion);
        }

        [HttpPut]
        [Route("documents/update")]
        public IHttpActionResult UpdaDocument([FromBody]DomainModel::Document document)
        {
            var updated = _documentService.UpdaDocument(document);

            return Ok(updated);
        }

        [HttpPost]
        [Route("documents/add")]
        public IHttpActionResult AddDocument([FromBody]DomainModel::Document document)
        {
            var addDocument = _documentService.AddDocument(document);

            return Ok(addDocument);
        }

        [HttpPost]
        [Route("documents/findversion/{version}")]
        public IHttpActionResult FindVersion([FromBody]DomainModel::Document document, int version)
        {
            var foundVersion = _documentService.FindVersion(document, version);

            return Ok(foundVersion);
        }


    }
}