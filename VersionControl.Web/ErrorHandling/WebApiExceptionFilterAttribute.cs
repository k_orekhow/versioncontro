﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Filters;

namespace VersionControl.Web.ErrorHandling
{
    public class WebApiExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public Type Type { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public override void OnException(HttpActionExecutedContext context)
        {

            var response = context.Request.CreateResponse<string>(StatusCode, context.Exception.ToString());
            throw new HttpResponseException(response);

        }
    }


}
