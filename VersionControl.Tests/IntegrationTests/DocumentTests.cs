﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VersionControl.DataAccess;
using VersionControl.Infrastructure;
using DomainModel= VersionControl.DomainModel;

namespace VersionControl.Tests
{
    [TestClass]
    public class UnitTest1
    {
        private IDocumentService _documentService;
        private DomainModel::Document _document1;
        private DomainModel::Document _updatedDoc;
        
       
        [TestInitialize]
        public void Start()
        {
           _documentService = new DocumentService(new DocumentRepository(), new DocumentVersionsRepository());
            var document1 = new DomainModel::Document()
            {
                Text = "Test text",
                Title = "Test title"
            };

            _document1 = _documentService.AddDocument(document1);
         
        }
        [TestMethod]
        public void SetActualTest()
        {
            _documentService.SetActual(_document1);
            var actualVersion = _documentService.GetActualVersion(_document1);

            Assert.AreEqual(_document1.Id, actualVersion.Id);
        }

        [TestMethod]
        public void UpdateDocumet()
        {
            _document1.Text = "New Test Text";
            _document1.Title = "New Test Title";
            _updatedDoc = _documentService.UpdaDocument(_document1);
            var actualVersion = _documentService.GetActualVersion(_document1);

            Assert.AreEqual(_updatedDoc.Id, actualVersion.Id);

            _documentService.SetActual(_document1);
            actualVersion = _documentService.GetActualVersion(_document1);

            Assert.AreEqual(_document1.Id, actualVersion.Id);

        }

        [TestMethod]
        public void DocumetVersions()
        {
            _document1.Text = "New Test Text";
            _document1.Title = "New Test Title";
            _updatedDoc = _documentService.UpdaDocument(_document1);
            var versionList = _documentService.GetDocumentVersionList(_document1);

            Assert.AreEqual(versionList!=null, true);
            Assert.AreEqual(versionList.Count>0, true);
            Assert.AreEqual(versionList.Count==2, true);
            Assert.AreEqual(versionList.Any(w=>w.Id==_updatedDoc.Id), true);
            Assert.AreEqual(versionList.Any(w =>w.Id == _document1.Id), true);
            Assert.AreNotEqual(versionList.Any(w =>w.Id == 1000), true);

        }

        [TestMethod]
        public void FindVersions()
        {
            _document1.Text = "New Test Text1";
            _document1.Title = "New Test Title1";
            _updatedDoc = _documentService.UpdaDocument(_document1);
            var versionList = _documentService.GetDocumentVersionList(_document1);
            var foundVersion = _documentService.FindVersion(_document1, versionList.Count - 1);
            //Restore in original state
            _document1 = _documentService.FindById(_document1.Id);

            Assert.IsNotNull(foundVersion);
            Assert.AreEqual(foundVersion.CompareTo(_updatedDoc)>0, true);

            foundVersion = _documentService.FindVersion(_document1, 0);

            Assert.IsNotNull(foundVersion);
            Assert.AreEqual(foundVersion.CompareTo(_document1)>0, true);

            Assert.AreNotEqual(foundVersion.CompareTo(new DomainModel::Document())>0, true);

        }

        [TestCleanup]
        public void CleanUp()
        {
            _documentService.DeleteDocument(_document1);
            if (_updatedDoc != null)
                _documentService.DeleteDocument(_updatedDoc);
        }
    }
}
