#VersionControl
Description goes here.

## Installation

- Open solution VersionControl.Server.sln in Visual Studio 2015.
- Enable package restore in Visual Studio http://stackoverflow.com/questions/27895504/how-do-i-enable-nuget-package-restore-in-visual-studio-2015.
- Build project.
- Project ready to use.

## Integration Tests

- Project VersionControl.Tests contains Integration test.

