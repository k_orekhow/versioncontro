﻿namespace VersionControl.DomainModel
{
    public class DocumentVersionPointer
    {
        public int Id { get; set;  }
        public int FirstVersionId { get; set; }
        public int ActualVersionId { get; set; }
    }
}
