﻿using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace VersionControl.DomainModel
{
    public static class StringExtensions
    {
        public static string GetMd5Hash(this string obj)
        {
            var md5 = MD5.Create();

            var inputBytes = Encoding.ASCII.GetBytes(obj);

            var hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string

            var sb = new StringBuilder();

            for (var i = 0; i < hash.Length; i++)
            {
                sb.Append(i.ToString("X2"));
            }

            return sb.ToString();
        }
    }
}
