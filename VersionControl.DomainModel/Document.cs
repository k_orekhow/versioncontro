﻿using System;

namespace VersionControl.DomainModel
{   
    public class Document:IComparable<Document>
    {
        public int Id { get; set; } = -1;
        public int DocumentVersion { get; set; } = 0;
        public int FirstVersionId { get; set; } = 0;
        public string Title { get; set; } = string.Empty;
        public string Text { get; set; } = string.Empty;
        public string Hash => (Title + Text).GetHashCode().ToString();
        public string OriginalHash { get; set; } = string.Empty;
        public int CompareTo(Document other)
        {
            var compareTo = Id == other.Id &&
                            DocumentVersion == other.DocumentVersion &&
                            FirstVersionId == other.FirstVersionId &&
                            Title == other.Title &&
                            Text == other.Text;

            return Convert.ToInt32(compareTo);
        }
    }
}
