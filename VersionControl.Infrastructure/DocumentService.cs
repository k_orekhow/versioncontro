﻿using System;
using System.Collections.Generic;
using System.Linq;
using Omu.ValueInjecter;
using VersionControl.DataAccess;
using Model = VersionControl.Model;
using DomainModel = VersionControl.DomainModel;

namespace VersionControl.Infrastructure
{
    public class DocumentService : IDocumentService
    {
        private IDocumentRepository _documentRepository;
        private IDocumentVersionsRepository _documentVersionsRepository;

        public DocumentService(IDocumentRepository documentRepository, IDocumentVersionsRepository documentVersionsRepository)
        {
            _documentRepository = documentRepository;
            _documentVersionsRepository = documentVersionsRepository;
        }

        public IList<DomainModel::Document> GetActualDocuments()
        {
            IEnumerable<int> documentVersions = _documentVersionsRepository.GetAll().Select(s=>s.Id);
            var dbDocuments = _documentRepository.Where(w => documentVersions.Contains(w.Id));
            var domainDocuments = dbDocuments.Select(s =>
            {
                var result = new DomainModel::Document().InjectFrom(s) as DomainModel::Document;
                if (result != null)
                    result.OriginalHash = s.Hash;
                return result;

            }).ToList();

            return domainDocuments;
        }

        public IList<DomainModel::Document> GetDocumentVersionList(DomainModel::Document document)
        {
            var result = new List<DomainModel::Document>();
            if (document != null)
            {
                var documents = _documentRepository
                    .Where(w => w.FirstVersionId == document.FirstVersionId)
                    .OrderBy(o=>o.Id)
                    .ToList();

                result = documents
                            .Select(s=>
                                    {
                                        var doc = new DomainModel::Document().InjectFrom(s) as DomainModel::Document;
                                        if(doc!=null)
                                            doc.OriginalHash = s.Hash;
                                        return doc;
                                    })
                            .ToList();
            }

            return result;
        }

        public bool SetActual(DomainModel::Document document)
        {

            Model::DocumentVersionPointer actualVersionPointer;
            actualVersionPointer = _documentVersionsRepository.FirstOrDefault(w=>w.FirstVersionId==document.FirstVersionId);

            if (actualVersionPointer == null)
            {
                actualVersionPointer = new Model::DocumentVersionPointer()
                {
                    ActualVersionId = document.Id,
                    FirstVersionId = document.FirstVersionId
                };

                _documentVersionsRepository.Add(actualVersionPointer);
            }
            else
            {
                actualVersionPointer.ActualVersionId = document.Id;
                _documentVersionsRepository.Update(actualVersionPointer);
            }

            return actualVersionPointer.ActualVersionId == document.Id;
        }

        public DomainModel::Document GetActualVersion(DomainModel::Document document)
        {
            DomainModel::Document result;
            var documentVersion = _documentVersionsRepository.FirstOrDefault(f => f.FirstVersionId == document.FirstVersionId);

            if (documentVersion == null)
            {
                SetActual(document);
                result = document;
            }
            else
            {
                var doc = _documentRepository.FirstOrDefault(w=>w.Id == documentVersion.ActualVersionId);   
                result = new DomainModel::Document().InjectFrom(doc) as DomainModel::Document;
                if (result != null)
                    result.OriginalHash = result.Hash;
            }

            return result;
        }

        public DomainModel::Document UpdaDocument(DomainModel.Document document)
        {
            var result = document;

            if (document.Hash == document.OriginalHash)
                return result;
           
            document.DocumentVersion = document.DocumentVersion + 1;
            result = AddDocument(document);

            return result;
        }

        public DomainModel::Document AddDocument(DomainModel.Document document)
        {
            var dbDocument = new Model::Document().InjectFrom(document) as Model::Document;
            var addDocument = _documentRepository.Add(dbDocument);

            var result = new DomainModel::Document().InjectFrom(addDocument) as DomainModel::Document;
            SetActual(result);

            if (result!=null)
            result.OriginalHash = addDocument.Hash;

            return result;
        }

        public DomainModel::Document DeleteDocument(DomainModel.Document document)
        {
            var dbDocument = new Model::Document().InjectFrom(document) as Model::Document;
            //Delete version pointer if found
            var versionPointer = _documentVersionsRepository.FirstOrDefault(w=>w.ActualVersionId == dbDocument.Id);
            if (versionPointer != null)
            {
                _documentVersionsRepository.Delete(versionPointer);
            }
            var delete = _documentRepository.Delete(dbDocument);

            return document;
        }

        public bool DeleteAllVersionPointer()
        {
            return _documentVersionsRepository.DeleteAll();
        }

        public DomainModel::Document FindVersion(DomainModel.Document document, int version)
        {
            var doc = _documentRepository.FirstOrDefault(
                w => w.FirstVersionId == document.FirstVersionId && w.DocumentVersion == version);

            var domainDoc = new DomainModel::Document().InjectFrom(doc) as DomainModel::Document;

            if (domainDoc != null)
                domainDoc.OriginalHash = doc.Hash;

            return domainDoc;
        }

        public DomainModel::Document FindById(int documentId)
        {
            var document = _documentRepository.FirstOrDefault(w => w.Id == documentId);
            var domainDoc = new DomainModel::Document().InjectFrom(document) as DomainModel::Document;
            if (domainDoc != null)
                domainDoc.OriginalHash = document.Hash;

            return domainDoc;
        }
    }
}
