﻿using System.Collections.Generic;
using DomainModel = VersionControl.DomainModel;

namespace VersionControl.Infrastructure
{
    public interface IDocumentService
    {
        IList<DomainModel::Document> GetActualDocuments();
        IList<DomainModel::Document> GetDocumentVersionList(DomainModel::Document document);
        bool SetActual(DomainModel::Document document);
        DomainModel::Document GetActualVersion(DomainModel::Document document);
        DomainModel::Document UpdaDocument(DomainModel::Document document);
        DomainModel::Document AddDocument(DomainModel::Document document);
        DomainModel::Document DeleteDocument(DomainModel::Document document);
        bool DeleteAllVersionPointer();
        DomainModel::Document FindVersion(DomainModel::Document document, int version);
        DomainModel::Document FindById(int documentId);


    }
}
