namespace VersionControl.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstVersionId = c.Int(nullable: false),
                        DocumentVersion = c.Int(nullable: false),
                        Title = c.String(),
                        Text = c.String(),
                        Hash = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocumentVersionPointers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstVersionId = c.Int(nullable: false),
                        ActualVersionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DocumentVersionPointers");
            DropTable("dbo.Documents");
        }
    }
}
