﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Model = VersionControl.Model;
using DomainModel = VersionControl.DomainModel;


namespace VersionControl.DataAccess
{
    public interface IDocumentRepository
    {
        Model::Document Add(Model::Document document);
        Model::Document Update(Model::Document document);
        Model::Document Delete(Model::Document document);
        Model::Document FindById(int id);
        IList<Model::Document> Where(Expression<Func<Model::Document, bool>> predicate);
        Model::Document FirstOrDefault(Expression<Func<Model::Document, bool>> predicate);
        IList<Model::Document> GetAll();
    }
}
