﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Omu.ValueInjecter;
using Model= VersionControl.Model;
using DomainModel = VersionControl.DomainModel;

namespace VersionControl.DataAccess
{
    public class DocumentRepository: GenericRepository<Model::Document>, IDocumentRepository
    {

        public Model::Document FindById(int id)
        {
            return Where(w => w.Id == id).FirstOrDefault();
        }

        public override Model.Document Add(Model.Document entity)
        {
            var document = base.Add(entity);

            if(entity.FirstVersionId == 0)
                document.FirstVersionId = document.Id;

            var update = Update(document);

            return update;
        }
    }
}
