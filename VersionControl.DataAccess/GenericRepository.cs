﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace VersionControl.DataAccess
{
    public class GenericRepository<T> where T:class 
    {
        public virtual T Add(T entity)
        {
            using (var dbContext = VersionControlDbContext.Create())
            {
                dbContext.Set<T>().Add(entity);
                dbContext.SaveChanges();
            }

            return entity;
        }

        public virtual T Update(T entity)
        {
            using (var dbContext = VersionControlDbContext.Create())
            {
                dbContext.Entry(entity).State = EntityState.Modified;
                //dbContext.Set<T>().Attach(entity);
                dbContext.SaveChanges();
            }

            return entity;
        }

        public virtual T Delete(T entity)
        {
            using (var dbContext = VersionControlDbContext.Create())
            {
                dbContext.Set<T>().Attach(entity);
                dbContext.Set<T>().Remove(entity);
                dbContext.SaveChanges();
            }

            return entity;
        }

        public virtual IList<T> Where(Expression<Func<T, bool>> predicate)
        {
            using (var dbContext = VersionControlDbContext.Create())
            {
                return dbContext.Set<T>().Where(predicate).ToList();
            }
        }

        public virtual T FirstOrDefault(Expression<Func<T, bool>> predicate)
        {
            using (var dbContext = VersionControlDbContext.Create())
            {
                return dbContext.Set<T>().FirstOrDefault(predicate);
            }
        }

        public virtual IList<T> GetAll()
        {
            using (var dbContext = VersionControlDbContext.Create())
            {
               return dbContext.Set<T>().ToList();
            }
        }
    }
}
