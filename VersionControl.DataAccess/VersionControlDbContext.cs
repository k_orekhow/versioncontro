﻿using System;
using System.Data.Entity;
using System.Transactions;
using VersionControl;
using Model=VersionControl.Model;

namespace VersionControl.DataAccess
{
    public class VersionControlDbContext : DbContext
    {
        public DbSet<Model::Document> Documents { get; set; }
        public DbSet<Model::DocumentVersionPointer> DocumentVersions { get; set; }

        public VersionControlDbContext()
               :base("DbConnection")
        {
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ValidateOnSaveEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
        }

        public override int SaveChanges()
        {
            var result = 0;
            try
            {
                using (
                    var transactionScope = new TransactionScope(
                        TransactionScopeOption.Suppress,
                        new TransactionOptions { IsolationLevel = IsolationLevel.Serializable }))
                {
                    ChangeTracker.DetectChanges();
                    result = base.SaveChanges();
                    transactionScope.Complete();
                }
            }
            catch (Exception e)
            {
                throw;
            }
            
            return result;
        }

        public static VersionControlDbContext Create()
        {
            return new VersionControlDbContext();
        }
    }
}
