﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Model = VersionControl.Model;
using DomainModel = VersionControl.DomainModel;


namespace VersionControl.DataAccess
{
    public interface IDocumentVersionsRepository
    {
        Model::DocumentVersionPointer Add(Model::DocumentVersionPointer document);
        Model::DocumentVersionPointer Update(Model::DocumentVersionPointer document);
        IList<Model::DocumentVersionPointer> Where(Expression<Func<Model::DocumentVersionPointer, bool>> predicate);
        Model::DocumentVersionPointer FirstOrDefault(Expression<Func<Model::DocumentVersionPointer, bool>> predicate);
        IList<Model::DocumentVersionPointer> GetAll();
        bool DeleteAll();
        Model::DocumentVersionPointer Delete(Model::DocumentVersionPointer versionPointer);
    }
}
