using System;
using System.Collections.Generic;
using System.Linq;
using Model= VersionControl.Model;
using DomainModel= VersionControl.DomainModel;


namespace VersionControl.DataAccess
{
    public class DocumentVersionsRepository : GenericRepository<Model::DocumentVersionPointer>, IDocumentVersionsRepository
    {
        public IList<Model::DocumentVersionPointer> GetAll()
        {
            using (var dbContext = VersionControlDbContext.Create())
            {
                var actualDocs = dbContext.DocumentVersions.ToList();

                return actualDocs;
            }
        }

        public bool DeleteAll()
        {
            using (var dbContext = VersionControlDbContext.Create())
            {
                var versionPointers = dbContext.DocumentVersions.ToList();
                var removeRange = dbContext.DocumentVersions.RemoveRange(versionPointers);
                var saveChanges = dbContext.SaveChanges();

                return saveChanges == versionPointers.Count;
            }
        }
    
    }
}
